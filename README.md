# KPI-MMTSD-3-Git

## Git Commands

- `git log --pretty=format:"%h - %an, %ar : %s"` - pretty format git log output
- `git log --since=2.weeks` - view commits over last 2 weeks
- `git checkout -b my_less` - create new branch called `my_less`